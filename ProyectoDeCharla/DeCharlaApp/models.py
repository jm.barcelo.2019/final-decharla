from django.db import models
from django.utils import timezone
# Create your models here.


class Session(models.Model):
    session_Cookie = models.TextField(unique=True)
    userName = models.TextField(default="Anonymous")
    size = models.IntegerField(default=1)
    font = models.TextField()


class Lobby(models.Model):
    name = models.CharField(max_length=64)
    number_msgs = models.IntegerField(default=0)



class Message(models.Model):
    lobby = models.ForeignKey(Lobby, on_delete=models.CASCADE)
    session = models.ForeignKey(Session, on_delete=models.CASCADE)
    text = models.TextField()
    is_img = models.BooleanField()
    date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return f"{self.lobby} - {self.session} - {self.text[:50]}"


class Vote(models.Model):
    lobby = models.ForeignKey(Lobby, on_delete=models.CASCADE)
    session = models.ForeignKey(Session, on_delete=models.CASCADE)
    value = models.CharField(max_length=10)


class Password(models.Model):
    password = models.TextField()

#sesion sala contraseña mensaje
