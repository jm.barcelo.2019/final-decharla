import json

from django.test import TestCase, Client
from django.urls import reverse
from .models import Session, Password, Lobby, Message, Vote


class LoginTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.session = Session.objects.create(session_Cookie="session_test")
        self.password = Password.objects.create(password='password')

    def test_login_unsuccessful(self):
        # "GET" to "/login"
        response = self.client.get(reverse("login"))

        # Answer code Verification
        self.assertEqual(response.status_code, 200)

        # Template Verification
        self.assertTemplateUsed(response, "login.html")

        # "POST" with an invalid password
        response = self.client.post(reverse("login"), {"password": "wrong_password"})

        # Answer Verification
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "login.html")


