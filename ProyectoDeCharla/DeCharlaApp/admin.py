from django.contrib import admin
from .models import Session, Password, Lobby, Message, Vote


# Register your models here.

admin.site.register(Session)
admin.site.register(Password)

admin.site.register(Lobby)

admin.site.register(Message)

admin.site.register(Vote)
