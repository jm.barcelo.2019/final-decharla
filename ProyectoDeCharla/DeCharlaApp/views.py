import secrets
import json
import xml.etree.ElementTree as ET
import http.client
import ssl
from itertools import zip_longest

from django.shortcuts import redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from .models import Session, Password, Message, Lobby, Vote

@csrf_exempt
def get_aemet_information(request):
    session_cookie = request.COOKIES.get('sessionid')

    check = Session.objects.filter(session_Cookie=session_cookie)
    if not check:
        return HttpResponseRedirect("/login")

    api_key = 'eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJqb3NlbWkxMEBsaXZlLmNvbSIsImp0aSI6IjMzNGMwMTUzLTU4MTgtNDNlZC04NDY2LTZiZGNiMjFjNjRhOSIsImlzcyI6IkFFTUVUIiwiaWF0IjoxNjg0NjI5MzAwLCJ1c2VySWQiOiIzMzRjMDE1My01ODE4LTQzZWQtODQ2Ni02YmRjYjIxYzY0YTkiLCJyb2xlIjoiIn0.zhbzOIbi-rbVSPhonAjI43OtLxuwPiiMZH96hFQFWAE'
    if request.method == "POST":
        ccaa = request.POST.get("ccaa")
        context = ssl.create_default_context()
        context.check_hostname = False
        context.verify_mode = ssl.CERT_NONE
        conn = http.client.HTTPSConnection("opendata.aemet.es", context=context)

        headers = {
            'cache-control': "no-cache"
        }

        conn.request("GET", f"/opendata/api/prediccion/ccaa/manana/{ccaa}?api_key={api_key}", headers=headers, )
        res = conn.getresponse()
        data = res.read().decode('utf-8', 'ignore')

        return HttpResponse(data, content_type='application/json')

    return render(request, "AEMET.html")


@csrf_exempt
def get_home(request):
    session_cookie = request.COOKIES.get('sessionid')

    # RECIBO EL VOTO --------------------------------
    if request.method == "POST":
        session = Session.objects.get(session_Cookie=session_cookie)  # Instancia de sesion
        value_vote = request.POST.get("vote")
        lobby_name = request.POST.get("lobby")
        lobby = Lobby.objects.get(name=lobby_name)  # Instancia de lobby
        check = Vote.objects.filter(lobby=lobby, session=session)

        if not check:
            vote = Vote(lobby=lobby, session=session, value=value_vote)
            vote.save()
        else:
            vote = Vote.objects.get(lobby=lobby, session=session)
            if value_vote != vote.value:
                vote.value = value_vote
                vote.save()

    # PETICION GET COMUN --------------------------------
    if session_cookie is None:
        cookie_value = secrets.token_hex(16)  # Genera una cadena hexadecimal de 16 bytes (32 caracteres)
        response = HttpResponseRedirect("/login")
        response.set_cookie('sessionid', cookie_value)
        return response
    else:
        check = Session.objects.filter(session_Cookie=request.COOKIES["sessionid"])
        if not check:
            return HttpResponseRedirect("/login")

    session = Session.objects.get(session_Cookie=session_cookie)

    lobby_list = Lobby.objects.all()
    vote_list = Vote.objects.filter(session=session)

    # ---------------------------------------
    likes_list = []
    dislikes_list = []
    for lobby in lobby_list:
        count_likes = 0
        count_dislikes = 0
        check_vote = Vote.objects.filter(lobby=lobby)
        if check_vote:
            for vote in check_vote:
                if vote.value == "like":
                    count_likes += 1
                else:
                    count_dislikes += 1

        likes_list.append(count_likes)
        dislikes_list.append(count_dislikes)
    # ---------------------------------------

    lobby_vote_list_comb = zip_longest(lobby_list, vote_list, likes_list, dislikes_list, fillvalue="")
    len_list = len(lobby_list)

    # Obtener imagenes y mensajes totales en las salas ---------
    num_images = 0
    num_messages = 0
    for lobby in lobby_list:
        messages_in_lobby = Message.objects.filter(lobby=lobby)
        for message in messages_in_lobby:
            if message.is_img == True:
                num_images += 1
            else:
                num_messages += 1

    return render(request, 'home.html', {"lobby_vote_list_comb": lobby_vote_list_comb, "len_list": len_list,
                                         "css": css_generator(session.font, session.size),
                                         "session": session, "num_messages": num_messages, "num_images": num_images})


@csrf_exempt
def login(request):
    if request.method == "POST":
        passwrd = request.POST["password"]
        check = Password.objects.filter(password=passwrd)
        if check:
            userName = request.POST["userName"]
            if userName == "":
                userName = "Anonymous"

            session_cookie = request.COOKIES.get('sessionid')

            c = Session(session_Cookie=session_cookie, userName=userName)
            c.save()
            return HttpResponseRedirect("/")
    return render(request, "login.html")


def logout(request):
    if request.method == "POST":
        session_cookie = request.POST["session_cookie"]
        session = Session.objects.get(session_Cookie=session_cookie)
        session.delete()

        return redirect("/")


def get_help(request):
    session_cookie = request.COOKIES.get('sessionid')
    check = Session.objects.filter(session_Cookie=request.COOKIES["sessionid"])
    if not check:
        return HttpResponseRedirect("/login")

    return render(request, "help.html")


def css_generator(font, font_size):
    css_content = f'''.body {{
        font-family: {font};
        font-size: {int(font_size)}rem;
    }}'''
    return css_content

@csrf_exempt
def get_configuration(request):
    session_cookie = request.COOKIES.get('sessionid')

    check = Session.objects.filter(session_Cookie=request.COOKIES["sessionid"])
    if not check:
        return HttpResponseRedirect("/login")

    session = Session.objects.get(session_Cookie=session_cookie)
    if request.method == 'POST':
        formulary = request.POST
        if "name" in formulary:
            name = request.POST["name"]
            session.userName = name
        if "size" in formulary:
            size = request.POST["size"]
            session.size = size
        if "font" in formulary:
            font = request.POST["font"]
            session.font = font

        session.save()
    return render(request, "configuration.html", {"css": css_generator(session.font, session.size)})


def get_favicon(request):
    favicon = open("favicon.ico", "rb")
    ans = favicon.read()
    return HttpResponse(ans)


def get_counter():
    counter = 1
    found = False
    if Lobby.objects.count() != 0:
        while not found:
            search_lobby = Lobby.objects.filter(name="lobby" + str(counter))
            if search_lobby:
                counter += 1
            else:
                found = True
    return counter


@csrf_exempt
def create_lobby(request):
    check = Session.objects.filter(session_Cookie=request.COOKIES["sessionid"])
    if not check:
        return HttpResponseRedirect("/login")

    if request.method == 'POST':
        name = request.POST["lobbyName"]
        if not name:
            name = "lobby" + str(get_counter())
        lobby = Lobby(name=name)
        lobby.save()
        return HttpResponseRedirect('/' + name)
    return render(request, "create.html")

@csrf_exempt
def delete_lobby(request):
    session_cookie = request.COOKIES.get('sessionid')
    check = Session.objects.filter(session_Cookie=request.COOKIES["sessionid"])
    if not check:
        return HttpResponseRedirect("/login")

    if request.method == 'POST':
        lobby_name = request.POST["lobby"]
        print(lobby_name)
        lobby = Lobby.objects.get(name=lobby_name)
        lobby.delete()
        return HttpResponseRedirect("/")


def xml_parser(xml_string):
    root = ET.fromstring(xml_string)
    messages = []
    for msg in root.findall('message'):
        isimg = msg.get('isimg')
        if isimg == "true":
            is_img = True
        else:
            is_img = False
        text = msg.find('text').text
        messages.append({'isimg': is_img, 'text': text})
    return messages


@csrf_exempt
def get_lobby(request, sala):
    check = Session.objects.filter(session_Cookie=request.COOKIES["sessionid"])
    if not check:
        return HttpResponseRedirect("/login")

    lobby = Lobby.objects.get(name=sala)  # Obtener instancia del modelo Lobby
    session_cookie = request.COOKIES.get('sessionid')
    session = Session.objects.get(session_Cookie=session_cookie)  # Obtener instancia del modelo Session

    # Receive and save the message -------------------------------------------
    if request.method == "PUT":
        xml_string = request.body.decode('utf-8')
        xml_messages = xml_parser(xml_string)
        for msg in xml_messages:
            message = Message(lobby=lobby, session=session, text=msg['text'], is_img=msg['isimg'])
            message.save()

    if request.method == "POST":
        message = request.POST["message"]
        img_check = request.POST.get("attach", False)
        if img_check:
            message = Message(lobby=lobby, session=session, text=message,
                              is_img=True)  # Asignar la instancia de Lobby al campo sala
        else:
            message = Message(lobby=lobby, session=session, text=message,
                              is_img=False)  # Asignar la instancia de Lobby al campo sala

        message.save()
    #  --------------------------------------------------------------

    # Actualizar el número de mensajes en el lobby
    lobby.number_msgs = Message.objects.filter(lobby=lobby).count()
    lobby.save()

    messages_list = Message.objects.filter(lobby=lobby)

    if request.path.endswith("din"):
        return render(request, "lobbydin.html",
                      {"lobby": lobby, "messages_list": messages_list, "session": session_cookie})

    else:
        return render(request, "lobby.html",
                      {"lobby": lobby, "messages_list": messages_list, "session": session_cookie})


def get_lobby_json(request, sala):
    check = Session.objects.filter(session_Cookie=request.COOKIES["sessionid"])
    if not check:
        return HttpResponseRedirect("/login")

    lobby = Lobby.objects.get(name=sala)
    messages_lobby = Message.objects.filter(lobby=lobby)

    messages_lobby_json = json.dumps(
        [{'author': msg.session.userName, 'text': msg.text, 'is_img': msg.is_img, 'date': msg.date.isoformat()}
         for msg in messages_lobby])
    return HttpResponse(messages_lobby_json, content_type='application/json')
