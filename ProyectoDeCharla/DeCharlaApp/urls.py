from django.urls import path
from . import views

from django.contrib import admin

urlpatterns = [
    path('', views.get_home, name="home"),
    path('aemet/weather', views.get_aemet_information),
    path('favicon.ico', views.get_favicon),
    path('login', views.login, name="login"),
    path('logout', views.logout),
    path('configuration', views.get_configuration),
    path('help', views.get_help),
    path('create', views.create_lobby),
    path('delete', views.delete_lobby),
    path('<str:sala>.json', views.get_lobby_json),
    path('<str:sala>', views.get_lobby),
    path('<str:sala>/din', views.get_lobby),



    #path('<str:sala>din/', views.get_lobbydin),
    #path('config/', views.get_config),


]
