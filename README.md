# Final DeCharla

# ENTREGA CONVOCATORIA MAYO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Jose Miguel Barcelo Garcia
* Titulación: Doble grado en Ingenieria en Tecnologias de la Telecomunicacion + Ingenieria Aeroespacial en Aeronavegacion
* Cuenta en laboratorios: josemi
* Cuenta URJC: jm.barcelo.2019
* Video básico (url): https://youtu.be/SNPqn7RLO24
* Video parte opcional (url): https://youtu.be/ombTlgwvP-0
* Despliegue (url): http://josemi1607.pythonanywhere.com/
* Contraseñas:
	Para acceder a la aplicacion DeCharlaApp: sat
* Cuenta Admin Site: jm.barcelo.2019/admin

## Resumen parte obligatoria
La siguiente aplicación es una plataforma de comunicación en tiempo real que permite a las diferentes sesiones identificadas chatear entre sí. Para acceder a los recursos de esta aplicación, se requiere autenticarse mediante una contraseña válida.

Una vez que se introduce la contraseña en la sección de "Inicio de sesión", los usuarios son dirigidos a la página principal (HOME), donde se encuentran las siguientes características:

    Varias opciones de navegación (CHATS, AYUDA, CONFIGURACIÓN y ADMIN).
    Una barra lateral que muestra información de la sesión actual.
    Una lista de las salas activas con enlaces a su estado estático.
    Además de los botones de la barra de navegación, tenemos el botón "CREAR CHAT", que redirige a una página donde se puede crear una sala con un nombre elegido por el usuario.
    Un pie de página que muestra el número total de mensajes, salas activas y el número total de imágenes, visible en la pagina principal de la aplicación.


Una vez que se accede a una sala de CHAT, ya sea estática o dinámica, se tiene la opción de enviar mensajes o imágenes. Para enviar una imagen, se debe incluir la URL de la imagen en el formulario y hacer clic en el botón correspondiente. Los mensajes y las imágenes enviadas por el usuario aparecerán con un formato diferente a los mensajes enviados por otras sesiones.

Además, en las salas estáticas de CHAT, se puede realizar una operación "PUT" incluyendo una serie de mensajes en formato "XML". Esta operación solo se puede realizar si se incluyen las cookies de la sesión en las cabeceras del "PUT" con la etiqueta "Cookie", y se incluye el token CSRF con la etiqueta "X-CSRFToken".

Los campos necesarios en el "XML" son los siguientes:

    Encabezado del formato XML.
    Campo "messages": Contiene todos los mensajes que se deseen incluir.
    Campo "message": Contiene el mensaje que se desea incluir y debe incluir la etiqueta "isimg", que puede tener el valor "True" si el mensaje es una imagen o "False" si el mensaje es texto.
    Campo "text": Contiene el texto del mensaje o la URL de la imagen.

Al hacer clic en el botón de AYUDA, se mostrará una página con explica el funcionamiento de la aplicación.

Al hacer clic en el botón de CONFIGURACIÓN, se mostrará una página donde se puede personalizar el nombre de la sesión, el tipo de letra y el tamaño de letra de la aplicación.

Por último, al hacer clic en el botón de ADMIN, se abrirá la interfaz del "Admin Site" característico de las aplicaciones DJANGO.

Además de los botones de navegación, en la barra lateral derecha se encuentra el botón "CREAR CHAT", que redirige a una página donde se puede crear una sala con un nombre elegido por el usuario.

En resumen, esta aplicación de chat facilita la interacción en tiempo real entre usuarios mediante salas de chat, permitiendo el envío de mensajes de texto e imágenes. Su diseño intuitivo y características adicionales, como la personalización y la administración de recursos, mejoran la experiencia del usuario.

## Lista partes opcionales

* Nombre parte: Favicon -> Se incluye un favicon en todos los recursos de nuestra aplicación.
* Nombre parte: Borrar sala -> El usuario va a poder eliminar cualquiera de los Chat creados a partir de un botón 'Delete Chat'.
* Nombre parte: Logout -> Boton que `ermite cerrar la sesion del usuario
* Nombre parte: API AEMET -> Acceso a la API de la AEMET paraobtener informacion meteorologia de la respectiva comunidad autonoma (CCAA)
* Nombre parte: API NASA (APOD) -> Acceso a la API de la NASA (APOD) para obtener la imagen del dia
* Nombre parte: Votar sala -> Permite a un usuario realizar votos sobre una sala
